
import {defineStore} from "pinia";
import axios from "axios";
import {ref} from "vue";

export const useHome = defineStore("home",()=>{
    const listUnitType = ref();
    async function getUnitType(){
        let config = {
          method: 'get',
          maxBodyLength: Infinity,
          url: 'http://localhost:8080/api/unit-types',
          headers: { }
        };

        await axios.request(config)
        .then((response) => {
          listUnitType.value = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });
    }
    const unitType = ref();

      async function getUnitTypeById(Id:any){
            let config = {
              method: 'get',
              maxBodyLength: Infinity,
              url: 'http://localhost:8080/api/unit-types/'+Id,
              headers: { }
            };

           await axios.request(config)
            .then((response) => {
              unitType.value = response.data.data;
            })
            .catch((error) => {
              console.log(error);
            });
      }

    return{
        getUnitType,
        listUnitType,
        unitType,
        getUnitTypeById

    }
})
