import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Test2View from "@/views/Test2View.vue";
import UnitTypeView from "@/views/UnitTypeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/test-name/:name',
      name: 'test-name',
      component: Test2View,
      props: true
    },
    {
      path: '/test-id/:id',
      name: 'test-id',
      component: Test2View,
      props: true
    },
      {
      path: '/unit-type',
      name: 'unit-type',
      component: UnitTypeView,
      props: true
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
